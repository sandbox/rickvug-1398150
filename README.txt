
-- SUMMARY --

Default Permissions is a simple module designed for distribution developers who
want to manage a default list of module permissions in a single location that
is easy to maintain. It does not provide a UI of any kind.

This module was developed because using Features to manage default permissions
in the context of a distribution is problematic as Features introduces
dependencies and permissions are "overridden" if they change from the provided
defaults. Developers interested in this module may also want to review
http://drupal.org/project/defaultconfig, which solves the problem with overrides
but isn't suitable for centralizing all default permissions into a single
module without introducing dependencies.


-- REQUIREMENTS --

* None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- USAGE --

In your custom module implement hook_default_permissions() and return a nested
array of permissions according to the following pattern:

$permissions['poll']['inspect all votes'] = array('editor', 'contributor');

Each time a module is enabled this hook will fire. If the module being enabled
matches a module key in the $permissions array the corresponding permission(s)
will be granted to the role(s) specified.

The key (in this example 'poll') is normally a module but can be anything. For
example Default Permissions supports reacting to content type creation with keys
in the format 'content_type_article', where 'article' is the content type. Your
module can pass an arbitrary array of keys to
default_permissions_grant_permissions() to try for matches. To allow for more
complex logic the $keys array passed to hook_default_permissions() so that hook
implementation can be aware of which keys are being passed to it.


-- CONTACT --

Current maintainers:
* Rick Vugteveen (rickvug) - http://drupal.org/user/42239

This project has been sponsored by:
* Dennis Publishing (http://dennis.co.uk)
